#!/usr/bin/env sh

if [$(whoami) = "root"];

	then 

		echo "[...] update database [...]" 

		apt-get update

		echo "[...] upgrade system  [...]"

		apt-get upgrade

	else

		echo "[/!\] Vous devez être super-utilisateur [/!\]"

fi

