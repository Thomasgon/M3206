#!/usr/bin/env sh


echo "[...] Checking Internet connection [...]"

ping -c3 8.8.8.8 > /dev/null
TEST=$?


if [ $TEST -ne 0 ];

then
	echo "[/!\] Not connected to Internet    [/!\]"
	echo "[/!\] Please check configuration   [/!\]"
else

	echo "[...] Internet access OK          [...]"
fi

echo "Fin du premier script"
